<?php
/**
 * Options for the filelist plugin
 */

$conf['cleancsvfile'] = 'cleanfullstaff.csv';
$conf['nonteacherscsvfile'] = 'nonteacherstaff.csv';
$conf['cleanrefis'] = 'cleanrefis.csv';
$conf['cleanteachers'] = 'cleanteachers.csv';
$conf['outfile'] = '.:lehrerposter.jpg';
$conf['delimiter'] = ',';
$conf['printtableheader'] = 0;
$conf['cleanfields'] = "Familienname,Vornamen,Kürzel,Telefonnummer Person Privat,E-Mail Person Dienstlich,Fächer";
$conf['createpublicjson'] = 0;
$conf['publicfields'] = "Familienname,Vornamen,Kürzel,E-Mail Person Dienstlich,Fächer";
