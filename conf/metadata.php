<?php
/**
 * Metadata for configuration manager plugin
 * Additions for the filelist plugin
 *
 * @author Gina Haeussge <osd@foosel.net>
 */

$meta['cleancsvfile'] = array('string');
$meta['nonteacherscsvfile'] = array('string');
$meta['cleanrefis'] = array('string');
$meta['cleanteachers'] = array('string');
$meta['outfile'] = array('string');
$meta['printtableheader'] = array('onoff');
$meta['delimiter'] = array('string');
$meta['cleanfields'] = array('string');
$meta['createpublicjson'] = array('onoff');
$meta['publicfields'] = array('string');
