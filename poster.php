<?php


class Poster {
    public $posterColumns = 10;
    public $singlePicWidth = 800;
    public $singlePicHeight = 1200;
    public $canvasWidth = 10000;
    public $posterTilePadding = 160;
    public $singlePicBottomspace = 180;
    public $fontName = '';
    public $outfile = '';
    public $fontSize = 48;
    public $numRows;

    function __construct($columns, $picwidth, $picheight, $fontname, $outfile) {
        // set dimensions
        $this->columns = $columns;
        $this->singePicWidth = $picwidth;
        $this->singlePicHeight = $picheight;
        $this->fontName = $fontname;
        $this->outfile = $outfile;
    }

    function create ($staffarray, $nonteacherstaffarray, $mediadir, $schoolname) {

        $tilenum = count($staffarray);
        $this->numRows = floor($tilenum/$this->posterColumns) + 3;
        $this->canvasWidth = $this->posterColumns * ($this->singlePicWidth + 2 * $this->posterTilePadding);

        $picsdir=$mediadir.str_replace(':', '/',cleanID(getNS(getID())))."/img";

        $canvas = $this->_createCanvas();


        $header = $this->_createHeader($schoolname, $picsdir);
        imagecopy($canvas,$header,0,0,0,0,$this->canvasWidth, $this->singlePicHeight/2);
        
        $nonteacherline = $this->_createNonTeacherLine($nonteacherstaffarray,$picsdir);

        $yoff= 170;
        imagecopy($canvas,$nonteacherline,0,$this->singlePicHeight/2+$yoff,0,0,$this->canvasWidth, $this->singlePicHeight + $this->singlePicBottomspace);

        $column=0;
        $row=0;
        $tilecount=0;
        
        $yoff= 120;
        foreach ($staffarray as $key => $line) {

            // fixme find index dynamically
            $namestring = $line[1] . " " . $line[0];
            $faecher = $line[3];
            $kuerzelfile = $this->_specialchars(strtolower($line[4]));
            
            // skip first line            
            if ( $tilecount == 0) {
                $namestring = "";
                $faecher = "";
                $kuerzelfile = "label_lehrer";
                $tilecount++;
            }

            $tile = $this->_createTile($kuerzelfile, $namestring, $faecher, $picsdir);
            // teacherimage ins posterimage kopieren
            $x=$column*($this->singlePicWidth + 2 * $this->posterTilePadding) + $this->posterTilePadding;
            $y=($row+1)*($this->singlePicHeight+$this->singlePicBottomspace+$this->posterTilePadding) + $this->posterTilePadding + $this->singlePicHeight/2 + $yoff;
            imagecopy($canvas, $tile, $x, $y, 0, 0, $this->singlePicWidth, $this->singlePicHeight + $this->singlePicBottomspace);
            
            $column++;
            if ($column >= $this->posterColumns) {
                $column = 0;
                $row++;
            }
        }

        $heute = date("m.d.Y");
        $created = "Erzeugt am $heute";
        imagettftext($canvas, 30, 90, $this->canvasWidth-60, $this->canvasHeight-600  , $black, $this->fontName, $created);
        imagejpeg ( $canvas , $this->outfile); 

    }    

    function _createNonTeacherLine($nonteacherstaffarray, $picsdir) {
        $height = $this->singlePicHeight + $this->singlePicBottomspace;
        $width = $this->canvasWidth;
        $nts = imagecreatetruecolor($width, $height);
        $black = imagecolorallocate($nts, 0, 0, 0);
        $bgcolor = imagecolorallocate($nts, 255, 255, 255);

        imagefill($nts, 0, 0, $bgcolor);

        $column=0;
        foreach ($nonteacherstaffarray as $line => $single) {
            foreach ($single as $k => $v) {
                $tile = $this->_createTile($single["pic"], $single["line1"], $single["line2"], $picsdir);
                $x=$column*($this->singlePicWidth + 2 * $this->posterTilePadding) + $this->posterTilePadding;
                imagecopy($nts, $tile, $x, 0, 0, 0, $this->singlePicWidth, $this->singlePicHeight + $this->singlePicBottomspace);
            }
            $column++;
        }

        return $nts;

    }

    function _createHeader($schoolname, $picsdir) {
        $headHeight = $this->singlePicHeight/2;
        $headWidth = $this->canvasWidth;

        $header = imagecreatetruecolor($headWidth, $headHeight);
        $black = imagecolorallocate($header, 0, 0, 0);
        $bgcolor = imagecolorallocate($header, 255, 255, 255);
        imagefill($header, 0, 0, $bgcolor);
        $picfile = $picsdir . "/" . "schullogo.jpg";
        $logo = imagecreatefromjpeg($picfile);


        imagettftext($header, 200, 0, $this->posterTilePadding, $this->singlePicHeight/2.8  , $black, $this->fontName, $schoolname);
        imagecopy($header, $logo, $this->canvasWidth-550, 40, 0, 0, 472, 550);

        return $header;

    }

    /**
     * Erzeugt eine Lehrerkachel mit Beschriftung
     */

    function _createTile ($imgname, $textLineOne, $textLineTwo, $picsdir) {

        if ($textLineOne == "label" ) $textLineOne ="";
        if ($textLineTwo == "label" ) $textLineTwo ="";
        $tile = imagecreatetruecolor($this->singlePicWidth, $this->singlePicHeight + $this->singlePicBottomspace);

        $black = imagecolorallocate($tile, 0, 0, 0);
        $bgcolor = imagecolorallocate($tile, 255, 255, 255);
    
        imagefill($tile, 0, 0, $bgcolor);

        $picfile = $picsdir . "/" . $imgname . ".jpg";

        if ( ! is_file($picfile)) {
            $picfile = $picsdir . "/" . "nopic.jpg";;
        }
        
        // Kürzelbild "laden"
        $photo = imagecreatefromjpeg($picfile);
        // Kürzelbild ins Kachelbild kopieren
        imagecopy($tile, $photo, 0, 0, 0, 0,  $this->singlePicWidth, $this->singlePicHeight);

        // Boundig box des Namenstexts ermitteln
        $bbox = imagettfbbox($this->fontSize, 0, $this->fontName, $textLineOne);
        $x = $bbox[0] + (imagesx($tile) / 2) - ($bbox[4] / 2);
        $y = $bbox[1] + imagesy($tile) - $this->singlePicBottomspace - $bbox[5];
        // Namenstext schreiben
        imagettftext($tile, $this->fontSize, 0, $x, $y, $black, $this->fontName, $textLineOne);

        // Boundig box des Fächertexts ermitteln
        $bbox = imagettfbbox($this->fontSize, 0, $this->fontName, $textLineTwo);
        $x = $bbox[0] + (imagesx($tile) / 2) - ($bbox[4] / 2);
        $y = $bbox[1] + imagesy($tile) - $this->singlePicBottomspace - $bbox[5] + $this->fontSize*1.7;
        // Fächertext schreiben
        imagettftext($tile, $this->fontSize, 0, $x, $y, $black, $this->fontName, $textLineTwo);


        return $tile;

    }
    /**
     * Erzeugt die Leinwand
     */
    function _createCanvas () {
        $this->canvasHeight = ($this->singlePicHeight+$this->singlePicBottomspace*1.7)*$this->numRows;
        $canvas  = imagecreatetruecolor($this->canvasWidth, $this->canvasHeight);
        $bgcolor = imagecolorallocate($canvas, 255, 255, 255);
        imagefill($canvas, 0, 0, $bgcolor);
        return $canvas;

    }

    function _specialchars($string) {
        $string = str_replace("ä", "ae", $string);
        $string = str_replace("ü", "ue", $string);
        $string = str_replace("ö", "oe", $string);
        $string = str_replace("Ä", "Ae", $string);
        $string = str_replace("Ü", "Ue", $string);
        $string = str_replace("Ö", "Oe", $string);
        $string = str_replace("ß", "ss", $string);
        return $string;
    }




}

?>